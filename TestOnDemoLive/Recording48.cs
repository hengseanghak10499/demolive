﻿///////////////////////////////////////////////////////////////////////////////
//
// This file was automatically generated by RANOREX.
// DO NOT MODIFY THIS FILE! It is regenerated by the designer.
// All your modifications will be lost!
// http://www.ranorex.com
//
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using System.Drawing;
using System.Threading;
using WinForms = System.Windows.Forms;

using Ranorex;
using Ranorex.Core;
using Ranorex.Core.Testing;
using Ranorex.Core.Repository;

namespace TestOnDemoLive
{
#pragma warning disable 0436 //(CS0436) The type 'type' in 'assembly' conflicts with the imported type 'type2' in 'assembly'. Using the type defined in 'assembly'.
    /// <summary>
    ///The Recording48 recording.
    /// </summary>
    [TestModule("1cd28c4e-127c-4246-b0c3-2b52445cfb11", ModuleType.Recording, 1)]
    public partial class Recording48 : ITestModule
    {
        /// <summary>
        /// Holds an instance of the TestOnDemoLiveRepository repository.
        /// </summary>
        public static TestOnDemoLiveRepository repo = TestOnDemoLiveRepository.Instance;

        static Recording48 instance = new Recording48();

        /// <summary>
        /// Constructs a new instance.
        /// </summary>
        public Recording48()
        {
            customerid = "550";
            givingvalue = "";
            payment = "";
        }

        /// <summary>
        /// Gets a static instance of this recording.
        /// </summary>
        public static Recording48 Instance
        {
            get { return instance; }
        }

#region Variables

        string _customerid;

        /// <summary>
        /// Gets or sets the value of variable customerid.
        /// </summary>
        [TestVariable("9c0443b7-877f-456e-ae41-54a0614b0f35")]
        public string customerid
        {
            get { return _customerid; }
            set { _customerid = value; }
        }

        string _givingvalue;

        /// <summary>
        /// Gets or sets the value of variable givingvalue.
        /// </summary>
        [TestVariable("a770b01c-c288-4799-87b7-b7ed209e7d6f")]
        public string givingvalue
        {
            get { return _givingvalue; }
            set { _givingvalue = value; }
        }

        string _payment;

        /// <summary>
        /// Gets or sets the value of variable payment.
        /// </summary>
        [TestVariable("957ea07e-9529-4335-b130-320ab2a3d2b3")]
        public string payment
        {
            get { return _payment; }
            set { _payment = value; }
        }

#endregion

        /// <summary>
        /// Starts the replay of the static recording <see cref="Instance"/>.
        /// </summary>
        [System.CodeDom.Compiler.GeneratedCode("Ranorex", global::Ranorex.Core.Constants.CodeGenVersion)]
        public static void Start()
        {
            TestModuleRunner.Run(Instance);
        }

        /// <summary>
        /// Performs the playback of actions in this recording.
        /// </summary>
        /// <remarks>You should not call this method directly, instead pass the module
        /// instance to the <see cref="TestModuleRunner.Run(ITestModule)"/> method
        /// that will in turn invoke this method.</remarks>
        [System.CodeDom.Compiler.GeneratedCode("Ranorex", global::Ranorex.Core.Constants.CodeGenVersion)]
        void ITestModule.Run()
        {
            Mouse.DefaultMoveTime = 300;
            Keyboard.DefaultKeyPressTime = 20;
            Delay.SpeedFactor = 1.00;

            Init();

            Report.Log(ReportLevel.Info, "Set value", "Setting attribute Text to '$customerid' on item 'CloseCounterSessionDialog.TxtSearch'.", repo.CloseCounterSessionDialog.TxtSearchInfo, new RecordItemIndex(0));
            repo.CloseCounterSessionDialog.TxtSearch.Element.SetAttributeValue("Text", customerid);
            Delay.Milliseconds(0);
            
            Report.Log(ReportLevel.Info, "Mouse", "Mouse Left Click item 'CloseCounterSessionDialog.Button' at 10;8.", repo.CloseCounterSessionDialog.ButtonInfo, new RecordItemIndex(1));
            repo.CloseCounterSessionDialog.Button.Click("10;8");
            Delay.Milliseconds(0);
            
            givingvalue = conver();
            Delay.Milliseconds(0);
            
            Report.Log(ReportLevel.Info, "Validation", "Validating AttributeEqual (Text=$givingvalue) on item 'CloseCounterSessionDialog.ទកបរកRow0'.", repo.CloseCounterSessionDialog.ទកបរកRow0Info, new RecordItemIndex(3));
            Validate.AttributeEqual(repo.CloseCounterSessionDialog.ទកបរកRow0Info, "Text", givingvalue);
            Delay.Milliseconds(100);
            
            Report.Log(ReportLevel.Info, "Mouse", "Mouse Left Click item 'CloseCounterSessionDialog.Button' at 11;11.", repo.CloseCounterSessionDialog.ButtonInfo, new RecordItemIndex(4));
            repo.CloseCounterSessionDialog.Button.Click("11;11");
            Delay.Milliseconds(0);
            
            Report.Log(ReportLevel.Info, "Mouse", "Mouse Left Click item 'CloseCounterSessionDialog.Button' at 6;10.", repo.CloseCounterSessionDialog.ButtonInfo, new RecordItemIndex(5));
            repo.CloseCounterSessionDialog.Button.Click("6;10");
            Delay.Milliseconds(0);
            
        }

#region Image Feature Data
#endregion
    }
#pragma warning restore 0436
}
