﻿///////////////////////////////////////////////////////////////////////////////
//
// This file was automatically generated by RANOREX.
// DO NOT MODIFY THIS FILE! It is regenerated by the designer.
// All your modifications will be lost!
// http://www.ranorex.com
//
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using System.Drawing;
using System.Threading;
using WinForms = System.Windows.Forms;

using Ranorex;
using Ranorex.Core;
using Ranorex.Core.Testing;
using Ranorex.Core.Repository;

namespace TestOnDemoLive
{
#pragma warning disable 0436 //(CS0436) The type 'type' in 'assembly' conflicts with the imported type 'type2' in 'assembly'. Using the type defined in 'assembly'.
    /// <summary>
    ///The R1114 recording.
    /// </summary>
    [TestModule("8f6fb498-71b9-4645-a02c-7870426eda70", ModuleType.Recording, 1)]
    public partial class R1114 : ITestModule
    {
        /// <summary>
        /// Holds an instance of the TestOnDemoLiveRepository repository.
        /// </summary>
        public static TestOnDemoLiveRepository repo = TestOnDemoLiveRepository.Instance;

        static R1114 instance = new R1114();

        /// <summary>
        /// Constructs a new instance.
        /// </summary>
        public R1114()
        {
            GetDate = "";
            Code_Cashier = "";
        }

        /// <summary>
        /// Gets a static instance of this recording.
        /// </summary>
        public static R1114 Instance
        {
            get { return instance; }
        }

#region Variables

        string _GetDate;

        /// <summary>
        /// Gets or sets the value of variable GetDate.
        /// </summary>
        [TestVariable("1ec9769c-82c2-47e9-b302-0bce00d3004e")]
        public string GetDate
        {
            get { return _GetDate; }
            set { _GetDate = value; }
        }

        string _Code_Cashier;

        /// <summary>
        /// Gets or sets the value of variable Code_Cashier.
        /// </summary>
        [TestVariable("f2a76833-87a7-4afe-8f11-933c600e16df")]
        public string Code_Cashier
        {
            get { return _Code_Cashier; }
            set { _Code_Cashier = value; }
        }

#endregion

        /// <summary>
        /// Starts the replay of the static recording <see cref="Instance"/>.
        /// </summary>
        [System.CodeDom.Compiler.GeneratedCode("Ranorex", global::Ranorex.Core.Constants.CodeGenVersion)]
        public static void Start()
        {
            TestModuleRunner.Run(Instance);
        }

        /// <summary>
        /// Performs the playback of actions in this recording.
        /// </summary>
        /// <remarks>You should not call this method directly, instead pass the module
        /// instance to the <see cref="TestModuleRunner.Run(ITestModule)"/> method
        /// that will in turn invoke this method.</remarks>
        [System.CodeDom.Compiler.GeneratedCode("Ranorex", global::Ranorex.Core.Constants.CodeGenVersion)]
        void ITestModule.Run()
        {
            Mouse.DefaultMoveTime = 300;
            Keyboard.DefaultKeyPressTime = 20;
            Delay.SpeedFactor = 1.00;

            Init();

            Report.Log(ReportLevel.Info, "Mouse", "Mouse Left Click item 'MainForm.MainPanel.BtnOpenCounter' at 45;8.", repo.MainForm.MainPanel.BtnOpenCounterInfo, new RecordItemIndex(0));
            repo.MainForm.MainPanel.BtnOpenCounter.Click("45;8");
            Delay.Milliseconds(0);
            
            Report.Log(ReportLevel.Info, "Mouse", "Mouse Left Click item 'OpenCounterSessionDialog.Open' at 10;12.", repo.OpenCounterSessionDialog.OpenInfo, new RecordItemIndex(1));
            repo.OpenCounterSessionDialog.Open.Click("10;12");
            Delay.Milliseconds(0);
            
            Report.Log(ReportLevel.Info, "Set value", "Setting attribute Text to '$Code_Cashier' on item 'SelectItemsDialog.TxtSearch'.", repo.SelectItemsDialog.TxtSearchInfo, new RecordItemIndex(2));
            repo.SelectItemsDialog.TxtSearch.Element.SetAttributeValue("Text", Code_Cashier);
            Delay.Milliseconds(0);
            
            Report.Log(ReportLevel.Info, "Mouse", "Mouse Left Click item 'SelectItemsDialog.Button' at 9;7.", repo.SelectItemsDialog.ButtonInfo, new RecordItemIndex(3));
            repo.SelectItemsDialog.Button.Click("9;7");
            Delay.Milliseconds(0);
            
            Report.Log(ReportLevel.Info, "Delay", "Waiting for 2s.", new RecordItemIndex(4));
            Delay.Duration(2000, false);
            
            Report.Log(ReportLevel.Info, "Mouse", "Mouse Left DoubleClick item 'SelectItemsDialog.ឈមRow1' at 120;23.", repo.SelectItemsDialog.ឈមRow1Info, new RecordItemIndex(5));
            repo.SelectItemsDialog.ឈមRow1.DoubleClick("120;23");
            Delay.Milliseconds(0);
            
            Report.Log(ReportLevel.Info, "Get Value", "Getting attribute 'AccessibleValue' from item 'OpenCounterSessionDialog.List10072020' and assigning its value to variable 'GetDate'.", repo.OpenCounterSessionDialog.List10072020Info, new RecordItemIndex(6));
            GetDate = repo.OpenCounterSessionDialog.List10072020.Element.GetAttributeValueText("AccessibleValue");
            Delay.Milliseconds(0);
            
            Report.Log(ReportLevel.Info, "Validation", "Validating AttributeEqual (AccessibleValue=$GetDate) on item 'OpenCounterSessionDialog.List10072020'.", repo.OpenCounterSessionDialog.List10072020Info, new RecordItemIndex(7));
            Validate.AttributeEqual(repo.OpenCounterSessionDialog.List10072020Info, "AccessibleValue", GetDate);
            Delay.Milliseconds(100);
            
            Report.Log(ReportLevel.Info, "Mouse", "Mouse Left Click item 'OpenCounterSessionDialog.BtnSave' at 77;10.", repo.OpenCounterSessionDialog.BtnSaveInfo, new RecordItemIndex(8));
            repo.OpenCounterSessionDialog.BtnSave.Click("77;10");
            Delay.Milliseconds(0);
            
            Report.Log(ReportLevel.Info, "Mouse", "Mouse Left Click item 'ExMessage.BtnYes' at 47;17.", repo.ExMessage.BtnYesInfo, new RecordItemIndex(9));
            repo.ExMessage.BtnYes.Click("47;17");
            Delay.Milliseconds(0);
            
        }

#region Image Feature Data
#endregion
    }
#pragma warning restore 0436
}
