﻿///////////////////////////////////////////////////////////////////////////////
//
// This file was automatically generated by RANOREX.
// DO NOT MODIFY THIS FILE! It is regenerated by the designer.
// All your modifications will be lost!
// http://www.ranorex.com
//
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using System.Drawing;
using System.Threading;
using WinForms = System.Windows.Forms;

using Ranorex;
using Ranorex.Core;
using Ranorex.Core.Testing;
using Ranorex.Core.Repository;

namespace TestOnDemoLive
{
#pragma warning disable 0436 //(CS0436) The type 'type' in 'assembly' conflicts with the imported type 'type2' in 'assembly'. Using the type defined in 'assembly'.
    /// <summary>
    ///The re1 recording.
    /// </summary>
    [TestModule("84bfbfa4-3712-4770-af6a-7beaa75603ce", ModuleType.Recording, 1)]
    public partial class Re1 : ITestModule
    {
        /// <summary>
        /// Holds an instance of the TestOnDemoLiveRepository repository.
        /// </summary>
        public static TestOnDemoLiveRepository repo = TestOnDemoLiveRepository.Instance;

        static Re1 instance = new Re1();

        /// <summary>
        /// Constructs a new instance.
        /// </summary>
        public Re1()
        {
        }

        /// <summary>
        /// Gets a static instance of this recording.
        /// </summary>
        public static Re1 Instance
        {
            get { return instance; }
        }

#region Variables

#endregion

        /// <summary>
        /// Starts the replay of the static recording <see cref="Instance"/>.
        /// </summary>
        [System.CodeDom.Compiler.GeneratedCode("Ranorex", global::Ranorex.Core.Constants.CodeGenVersion)]
        public static void Start()
        {
            TestModuleRunner.Run(Instance);
        }

        /// <summary>
        /// Performs the playback of actions in this recording.
        /// </summary>
        /// <remarks>You should not call this method directly, instead pass the module
        /// instance to the <see cref="TestModuleRunner.Run(ITestModule)"/> method
        /// that will in turn invoke this method.</remarks>
        [System.CodeDom.Compiler.GeneratedCode("Ranorex", global::Ranorex.Core.Constants.CodeGenVersion)]
        void ITestModule.Run()
        {
            Mouse.DefaultMoveTime = 300;
            Keyboard.DefaultKeyPressTime = 20;
            Delay.SpeedFactor = 1.00;

            Init();

            Report.Log(ReportLevel.Info, "Mouse", "Mouse Left Click item 'MainForm.MainPanel.StormBillingInterfacesDueDateRulePage' at 104;9.", repo.MainForm.MainPanel.StormBillingInterfacesDueDateRulePageInfo, new RecordItemIndex(0));
            repo.MainForm.MainPanel.StormBillingInterfacesDueDateRulePage.Click("104;9");
            Delay.Milliseconds(0);
            
            Report.Log(ReportLevel.Info, "Mouse", "Mouse Left Click item 'MainForm.MainPanel.TxtSearch3' at 95;11.", repo.MainForm.MainPanel.TxtSearch3Info, new RecordItemIndex(1));
            repo.MainForm.MainPanel.TxtSearch3.Click("95;11");
            Delay.Milliseconds(0);
            
            Report.Log(ReportLevel.Info, "Keyboard", "Key sequence '{LControlKey up}{LShiftKey down}{LMenu down}{LShiftKey up}{LMenu up}' with focus on 'MainForm.MainPanel.TxtSearch3'.", repo.MainForm.MainPanel.TxtSearch3Info, new RecordItemIndex(2));
            repo.MainForm.MainPanel.TxtSearch3.PressKeys("{LControlKey up}{LShiftKey down}{LMenu down}{LShiftKey up}{LMenu up}");
            Delay.Milliseconds(0);
            
        }

#region Image Feature Data
#endregion
    }
#pragma warning restore 0436
}
