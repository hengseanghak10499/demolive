﻿///////////////////////////////////////////////////////////////////////////////
//
// This file was automatically generated by RANOREX.
// DO NOT MODIFY THIS FILE! It is regenerated by the designer.
// All your modifications will be lost!
// http://www.ranorex.com
//
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using System.Drawing;
using System.Threading;
using WinForms = System.Windows.Forms;

using Ranorex;
using Ranorex.Core;
using Ranorex.Core.Testing;
using Ranorex.Core.Repository;

namespace TestOnDemoLive
{
#pragma warning disable 0436 //(CS0436) The type 'type' in 'assembly' conflicts with the imported type 'type2' in 'assembly'. Using the type defined in 'assembly'.
    /// <summary>
    ///The Recording16 recording.
    /// </summary>
    [TestModule("424d2b34-d030-4066-8f56-df0f9818ccfc", ModuleType.Recording, 1)]
    public partial class Recording16 : ITestModule
    {
        /// <summary>
        /// Holds an instance of the TestOnDemoLiveRepository repository.
        /// </summary>
        public static TestOnDemoLiveRepository repo = TestOnDemoLiveRepository.Instance;

        static Recording16 instance = new Recording16();

        /// <summary>
        /// Constructs a new instance.
        /// </summary>
        public Recording16()
        {
        }

        /// <summary>
        /// Gets a static instance of this recording.
        /// </summary>
        public static Recording16 Instance
        {
            get { return instance; }
        }

#region Variables

#endregion

        /// <summary>
        /// Starts the replay of the static recording <see cref="Instance"/>.
        /// </summary>
        [System.CodeDom.Compiler.GeneratedCode("Ranorex", global::Ranorex.Core.Constants.CodeGenVersion)]
        public static void Start()
        {
            TestModuleRunner.Run(Instance);
        }

        /// <summary>
        /// Performs the playback of actions in this recording.
        /// </summary>
        /// <remarks>You should not call this method directly, instead pass the module
        /// instance to the <see cref="TestModuleRunner.Run(ITestModule)"/> method
        /// that will in turn invoke this method.</remarks>
        [System.CodeDom.Compiler.GeneratedCode("Ranorex", global::Ranorex.Core.Constants.CodeGenVersion)]
        void ITestModule.Run()
        {
            Mouse.DefaultMoveTime = 300;
            Keyboard.DefaultKeyPressTime = 20;
            Delay.SpeedFactor = 1.00;

            Init();

            Report.Log(ReportLevel.Info, "Mouse", "Mouse Left Click item 'MainForm.PTopMenu.LblCaption6' at 40;14.", repo.MainForm.PTopMenu.LblCaption6Info, new RecordItemIndex(0));
            repo.MainForm.PTopMenu.LblCaption6.Click("40;14");
            Delay.Milliseconds(0);
            
            Report.Log(ReportLevel.Info, "Mouse", "Mouse Left Click item 'MainForm.MainPanel.Button7' at 8;10.", repo.MainForm.MainPanel.Button7Info, new RecordItemIndex(1));
            repo.MainForm.MainPanel.Button7.Click("8;10");
            Delay.Milliseconds(0);
            
            Report.Log(ReportLevel.Info, "Mouse", "Mouse Left Click item 'MainForm.ButtonTitle1' at 50;12.", repo.MainForm.ButtonTitle1Info, new RecordItemIndex(2));
            repo.MainForm.ButtonTitle1.Click("50;12");
            Delay.Milliseconds(0);
            
        }

#region Image Feature Data
#endregion
    }
#pragma warning restore 0436
}
