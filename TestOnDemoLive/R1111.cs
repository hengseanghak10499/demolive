﻿///////////////////////////////////////////////////////////////////////////////
//
// This file was automatically generated by RANOREX.
// DO NOT MODIFY THIS FILE! It is regenerated by the designer.
// All your modifications will be lost!
// http://www.ranorex.com
//
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using System.Drawing;
using System.Threading;
using WinForms = System.Windows.Forms;

using Ranorex;
using Ranorex.Core;
using Ranorex.Core.Testing;
using Ranorex.Core.Repository;

namespace TestOnDemoLive
{
#pragma warning disable 0436 //(CS0436) The type 'type' in 'assembly' conflicts with the imported type 'type2' in 'assembly'. Using the type defined in 'assembly'.
    /// <summary>
    ///The R1111 recording.
    /// </summary>
    [TestModule("f6abbd3d-fc91-4ceb-b429-c2e5e54a6c3f", ModuleType.Recording, 1)]
    public partial class R1111 : ITestModule
    {
        /// <summary>
        /// Holds an instance of the TestOnDemoLiveRepository repository.
        /// </summary>
        public static TestOnDemoLiveRepository repo = TestOnDemoLiveRepository.Instance;

        static R1111 instance = new R1111();

        /// <summary>
        /// Constructs a new instance.
        /// </summary>
        public R1111()
        {
            Code_Cashier = "";
            Name_Cashier = "";
            GetDate = "";
            Currency = "";
        }

        /// <summary>
        /// Gets a static instance of this recording.
        /// </summary>
        public static R1111 Instance
        {
            get { return instance; }
        }

#region Variables

        string _Code_Cashier;

        /// <summary>
        /// Gets or sets the value of variable Code_Cashier.
        /// </summary>
        [TestVariable("65710f40-93e6-4365-9c52-59024ba3dc39")]
        public string Code_Cashier
        {
            get { return _Code_Cashier; }
            set { _Code_Cashier = value; }
        }

        string _Name_Cashier;

        /// <summary>
        /// Gets or sets the value of variable Name_Cashier.
        /// </summary>
        [TestVariable("150e3060-0fa3-4251-8b80-850f9814b098")]
        public string Name_Cashier
        {
            get { return _Name_Cashier; }
            set { _Name_Cashier = value; }
        }

        string _GetDate;

        /// <summary>
        /// Gets or sets the value of variable GetDate.
        /// </summary>
        [TestVariable("323495e6-c116-4cfc-95c2-10b437cafcb2")]
        public string GetDate
        {
            get { return _GetDate; }
            set { _GetDate = value; }
        }

        string _Currency;

        /// <summary>
        /// Gets or sets the value of variable Currency.
        /// </summary>
        [TestVariable("21bd4a32-27d0-4f4d-95ab-782e3c83c638")]
        public string Currency
        {
            get { return _Currency; }
            set { _Currency = value; }
        }

        /// <summary>
        /// Gets or sets the value of variable Select_Row.
        /// </summary>
        [TestVariable("498b5313-9e21-4c63-a214-5b7fb1c6b0d6")]
        public string Select_Row
        {
            get { return repo.Select_Row; }
            set { repo.Select_Row = value; }
        }

#endregion

        /// <summary>
        /// Starts the replay of the static recording <see cref="Instance"/>.
        /// </summary>
        [System.CodeDom.Compiler.GeneratedCode("Ranorex", global::Ranorex.Core.Constants.CodeGenVersion)]
        public static void Start()
        {
            TestModuleRunner.Run(Instance);
        }

        /// <summary>
        /// Performs the playback of actions in this recording.
        /// </summary>
        /// <remarks>You should not call this method directly, instead pass the module
        /// instance to the <see cref="TestModuleRunner.Run(ITestModule)"/> method
        /// that will in turn invoke this method.</remarks>
        [System.CodeDom.Compiler.GeneratedCode("Ranorex", global::Ranorex.Core.Constants.CodeGenVersion)]
        void ITestModule.Run()
        {
            Mouse.DefaultMoveTime = 300;
            Keyboard.DefaultKeyPressTime = 20;
            Delay.SpeedFactor = 1.00;

            Init();

            Report.Log(ReportLevel.Info, "Delay", "Waiting for 1s.", new RecordItemIndex(0));
            Delay.Duration(1000, false);
            
            Report.Log(ReportLevel.Info, "Mouse", "Mouse Left Click item 'MainForm.MainPanel.BtnOpenCounter' at 55;15.", repo.MainForm.MainPanel.BtnOpenCounterInfo, new RecordItemIndex(1));
            repo.MainForm.MainPanel.BtnOpenCounter.Click("55;15");
            Delay.Milliseconds(0);
            
            Report.Log(ReportLevel.Info, "Mouse", "Mouse Left Click item 'OpenCounterSessionDialog.Open' at 6;16.", repo.OpenCounterSessionDialog.OpenInfo, new RecordItemIndex(2));
            repo.OpenCounterSessionDialog.Open.Click("6;16");
            Delay.Milliseconds(0);
            
            Report.Log(ReportLevel.Info, "Mouse", "Mouse Left DoubleClick item 'SelectItemsDialog.ឈមRow01' at 272;16.", repo.SelectItemsDialog.ឈមRow01Info, new RecordItemIndex(3));
            repo.SelectItemsDialog.ឈមRow01.DoubleClick("272;16");
            Delay.Milliseconds(0);
            
            Report.Log(ReportLevel.Info, "Mouse", "Mouse Left Click item 'CounterDialog.Open' at 5;11.", repo.CounterDialog.OpenInfo, new RecordItemIndex(4));
            repo.CounterDialog.Open.Click("5;11");
            Delay.Milliseconds(0);
            
            Report.Log(ReportLevel.Info, "Mouse", "Mouse Left DoubleClick item 'SelectItemsDialog.Copy_of_ឈមRow0' at 101;13.", repo.SelectItemsDialog.Copy_of_ឈមRow0Info, new RecordItemIndex(5));
            repo.SelectItemsDialog.Copy_of_ឈមRow0.DoubleClick("101;13");
            Delay.Milliseconds(0);
            
            Report.Log(ReportLevel.Info, "Set value", "Setting attribute Text to '$Code_Cashier' on item 'CounterDialog.Text2'.", repo.CounterDialog.Text2Info, new RecordItemIndex(6));
            repo.CounterDialog.Text2.Element.SetAttributeValue("Text", Code_Cashier);
            Delay.Milliseconds(0);
            
            Report.Log(ReportLevel.Info, "Set value", "Setting attribute Text to '$Name_Cashier' on item 'CounterDialog.Text'.", repo.CounterDialog.TextInfo, new RecordItemIndex(7));
            repo.CounterDialog.Text.Element.SetAttributeValue("Text", Name_Cashier);
            Delay.Milliseconds(0);
            
            Report.Log(ReportLevel.Info, "Mouse", "Mouse Left Click item 'CounterDialog.CboCurrency' at 123;10.", repo.CounterDialog.CboCurrencyInfo, new RecordItemIndex(8));
            repo.CounterDialog.CboCurrency.Click("123;10");
            Delay.Milliseconds(0);
            
            Report.Log(ReportLevel.Info, "Keyboard", "Key sequence from variable '$Currency' with focus on 'CounterDialog.CboCurrency'.", repo.CounterDialog.CboCurrencyInfo, new RecordItemIndex(9));
            repo.CounterDialog.CboCurrency.PressKeys(Currency);
            Delay.Milliseconds(0);
            
            Report.Log(ReportLevel.Info, "Keyboard", "Key sequence '{Return}' with focus on 'CounterDialog.CboCurrency'.", repo.CounterDialog.CboCurrencyInfo, new RecordItemIndex(10));
            repo.CounterDialog.CboCurrency.PressKeys("{Return}");
            Delay.Milliseconds(0);
            
            Report.Log(ReportLevel.Info, "Mouse", "Mouse Left Click item 'CounterDialog.BtnSave' at 48;10.", repo.CounterDialog.BtnSaveInfo, new RecordItemIndex(11));
            repo.CounterDialog.BtnSave.Click("48;10");
            Delay.Milliseconds(0);
            
            Report.Log(ReportLevel.Info, "Get Value", "Getting attribute 'AccessibleValue' from item 'OpenCounterSessionDialog.List10072020' and assigning its value to variable 'GetDate'.", repo.OpenCounterSessionDialog.List10072020Info, new RecordItemIndex(12));
            GetDate = repo.OpenCounterSessionDialog.List10072020.Element.GetAttributeValueText("AccessibleValue");
            Delay.Milliseconds(0);
            
            Report.Log(ReportLevel.Info, "Validation", "Validating AttributeEqual (AccessibleValue=$GetDate) on item 'OpenCounterSessionDialog.List10072020'.", repo.OpenCounterSessionDialog.List10072020Info, new RecordItemIndex(13));
            Validate.AttributeEqual(repo.OpenCounterSessionDialog.List10072020Info, "AccessibleValue", GetDate);
            Delay.Milliseconds(100);
            
            Report.Log(ReportLevel.Info, "User", GetDate, new RecordItemIndex(14));
            
            Report.Log(ReportLevel.Info, "User", Code_Cashier, new RecordItemIndex(15));
            
            Report.Log(ReportLevel.Info, "Mouse", "Mouse Left Click item 'OpenCounterSessionDialog.BtnSave' at 58;19.", repo.OpenCounterSessionDialog.BtnSaveInfo, new RecordItemIndex(16));
            repo.OpenCounterSessionDialog.BtnSave.Click("58;19");
            Delay.Milliseconds(0);
            
            Report.Log(ReportLevel.Info, "Mouse", "Mouse Left Click item 'ExMessage.BtnYes' at 53;15.", repo.ExMessage.BtnYesInfo, new RecordItemIndex(17));
            repo.ExMessage.BtnYes.Click("53;15");
            Delay.Milliseconds(0);
            
        }

#region Image Feature Data
#endregion
    }
#pragma warning restore 0436
}
