﻿///////////////////////////////////////////////////////////////////////////////
//
// This file was automatically generated by RANOREX.
// DO NOT MODIFY THIS FILE! It is regenerated by the designer.
// All your modifications will be lost!
// http://www.ranorex.com
//
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using System.Drawing;
using System.Threading;
using WinForms = System.Windows.Forms;

using Ranorex;
using Ranorex.Core;
using Ranorex.Core.Testing;
using Ranorex.Core.Repository;

namespace TestOnDemoLive
{
#pragma warning disable 0436 //(CS0436) The type 'type' in 'assembly' conflicts with the imported type 'type2' in 'assembly'. Using the type defined in 'assembly'.
    /// <summary>
    ///The Recording55 recording.
    /// </summary>
    [TestModule("144362c7-57da-4c49-b07f-ddfee8ab5a16", ModuleType.Recording, 1)]
    public partial class Recording55 : ITestModule
    {
        /// <summary>
        /// Holds an instance of the TestOnDemoLiveRepository repository.
        /// </summary>
        public static TestOnDemoLiveRepository repo = TestOnDemoLiveRepository.Instance;

        static Recording55 instance = new Recording55();

        /// <summary>
        /// Constructs a new instance.
        /// </summary>
        public Recording55()
        {
            BranchName = "KSP-HQ អគ្គិសនីកំពង់ស្ពឺ";
            CashName = "CASH1011";
            CounterType = "Normal counters";
        }

        /// <summary>
        /// Gets a static instance of this recording.
        /// </summary>
        public static Recording55 Instance
        {
            get { return instance; }
        }

#region Variables

        string _BranchName;

        /// <summary>
        /// Gets or sets the value of variable BranchName.
        /// </summary>
        [TestVariable("33a794b9-bc16-45f8-960f-6fa78ce3293f")]
        public string BranchName
        {
            get { return _BranchName; }
            set { _BranchName = value; }
        }

        string _CashName;

        /// <summary>
        /// Gets or sets the value of variable CashName.
        /// </summary>
        [TestVariable("6db50d39-ce88-4227-a1eb-24fce29c6699")]
        public string CashName
        {
            get { return _CashName; }
            set { _CashName = value; }
        }

        string _CounterType;

        /// <summary>
        /// Gets or sets the value of variable CounterType.
        /// </summary>
        [TestVariable("3e0d7944-c59e-4c22-8885-09ca55d54b18")]
        public string CounterType
        {
            get { return _CounterType; }
            set { _CounterType = value; }
        }

#endregion

        /// <summary>
        /// Starts the replay of the static recording <see cref="Instance"/>.
        /// </summary>
        [System.CodeDom.Compiler.GeneratedCode("Ranorex", global::Ranorex.Core.Constants.CodeGenVersion)]
        public static void Start()
        {
            TestModuleRunner.Run(Instance);
        }

        /// <summary>
        /// Performs the playback of actions in this recording.
        /// </summary>
        /// <remarks>You should not call this method directly, instead pass the module
        /// instance to the <see cref="TestModuleRunner.Run(ITestModule)"/> method
        /// that will in turn invoke this method.</remarks>
        [System.CodeDom.Compiler.GeneratedCode("Ranorex", global::Ranorex.Core.Constants.CodeGenVersion)]
        void ITestModule.Run()
        {
            Mouse.DefaultMoveTime = 300;
            Keyboard.DefaultKeyPressTime = 20;
            Delay.SpeedFactor = 1.00;

            Init();

            Report.Log(ReportLevel.Info, "Mouse", "Mouse Left Click item 'MainForm.PTopMenu.LblCaption51' at 9;11.", repo.MainForm.PTopMenu.LblCaption51Info, new RecordItemIndex(0));
            repo.MainForm.PTopMenu.LblCaption51.Click("9;11");
            Delay.Milliseconds(0);
            
            Report.Log(ReportLevel.Info, "Mouse", "Mouse Left Click item 'MainForm.ButtonTitle11' at 79;17.", repo.MainForm.ButtonTitle11Info, new RecordItemIndex(1));
            repo.MainForm.ButtonTitle11.Click("79;17");
            Delay.Milliseconds(0);
            
            Report.Log(ReportLevel.Info, "Mouse", "Mouse Left Click item 'MainForm.MainPanel.StormPaymentInterfacesCounterPage' at 70;19.", repo.MainForm.MainPanel.StormPaymentInterfacesCounterPageInfo, new RecordItemIndex(2));
            repo.MainForm.MainPanel.StormPaymentInterfacesCounterPage.Click("70;19");
            Delay.Milliseconds(0);
            
            Report.Log(ReportLevel.Info, "Mouse", "Mouse Left Click item 'MainForm.MainPanel.CboBranch1' at 97;13.", repo.MainForm.MainPanel.CboBranch1Info, new RecordItemIndex(3));
            repo.MainForm.MainPanel.CboBranch1.Click("97;13");
            Delay.Milliseconds(0);
            
            Report.Log(ReportLevel.Info, "Mouse", "Mouse Left Click item 'SelectItemsDialog.TxtSearch' at 109;9.", repo.SelectItemsDialog.TxtSearchInfo, new RecordItemIndex(4));
            repo.SelectItemsDialog.TxtSearch.Click("109;9");
            Delay.Milliseconds(0);
            
            Report.Log(ReportLevel.Info, "Set value", "Setting attribute Text to '$BranchName' on item 'SelectItemsDialog.TxtSearch'.", repo.SelectItemsDialog.TxtSearchInfo, new RecordItemIndex(5));
            repo.SelectItemsDialog.TxtSearch.Element.SetAttributeValue("Text", BranchName);
            Delay.Milliseconds(0);
            
            Report.Log(ReportLevel.Info, "Mouse", "Mouse Left Click item 'SelectItemsDialog.Button' at 11;5.", repo.SelectItemsDialog.ButtonInfo, new RecordItemIndex(6));
            repo.SelectItemsDialog.Button.Click("11;5");
            Delay.Milliseconds(0);
            
            Report.Log(ReportLevel.Info, "Mouse", "Mouse Left Click item 'SelectItemsDialog.ឈមRow1' at 138;19.", repo.SelectItemsDialog.ឈមRow1Info, new RecordItemIndex(7));
            repo.SelectItemsDialog.ឈមRow1.Click("138;19");
            Delay.Milliseconds(0);
            
            Report.Log(ReportLevel.Info, "Mouse", "Mouse Left Click item 'SelectItemsDialog.BtnSelect' at 38;15.", repo.SelectItemsDialog.BtnSelectInfo, new RecordItemIndex(8));
            repo.SelectItemsDialog.BtnSelect.Click("38;15");
            Delay.Milliseconds(0);
            
            Report.Log(ReportLevel.Info, "Mouse", "Mouse Left Click item 'MainForm.MainPanel.TxtSearch24' at 63;7.", repo.MainForm.MainPanel.TxtSearch24Info, new RecordItemIndex(9));
            repo.MainForm.MainPanel.TxtSearch24.Click("63;7");
            Delay.Milliseconds(0);
            
            Report.Log(ReportLevel.Info, "Set value", "Setting attribute Text to '$CashName' on item 'MainForm.MainPanel.TxtSearch24'.", repo.MainForm.MainPanel.TxtSearch24Info, new RecordItemIndex(10));
            repo.MainForm.MainPanel.TxtSearch24.Element.SetAttributeValue("Text", CashName);
            Delay.Milliseconds(0);
            
            Report.Log(ReportLevel.Info, "Mouse", "Mouse Left Click item 'MainForm.MainPanel.Button111' at 10;11.", repo.MainForm.MainPanel.Button111Info, new RecordItemIndex(11));
            repo.MainForm.MainPanel.Button111.Click("10;11");
            Delay.Milliseconds(0);
            
            Report.Log(ReportLevel.Info, "Mouse", "Mouse Left Click item 'MainForm.MainPanel.BtnUpdate21' at 40;13.", repo.MainForm.MainPanel.BtnUpdate21Info, new RecordItemIndex(12));
            repo.MainForm.MainPanel.BtnUpdate21.Click("40;13");
            Delay.Milliseconds(0);
            
            try {
                Report.Log(ReportLevel.Info, "Validation", "(Optional Action)\r\nValidating AttributeEqual (Text=$CashName) on item 'CounterDialog.Text'.", repo.CounterDialog.TextInfo, new RecordItemIndex(13));
                Validate.AttributeEqual(repo.CounterDialog.TextInfo, "Text", CashName, null, false);
                Delay.Milliseconds(100);
            } catch(Exception ex) { Report.Log(ReportLevel.Warn, "Module", "(Optional Action) " + ex.Message, new RecordItemIndex(13)); }
            
            try {
                Report.Log(ReportLevel.Info, "Validation", "(Optional Action)\r\nValidating AttributeEqual (Text=$CounterType) on item 'CounterDialog.CboCounterType'.", repo.CounterDialog.CboCounterTypeInfo, new RecordItemIndex(14));
                Validate.AttributeEqual(repo.CounterDialog.CboCounterTypeInfo, "Text", CounterType, null, false);
                Delay.Milliseconds(100);
            } catch(Exception ex) { Report.Log(ReportLevel.Warn, "Module", "(Optional Action) " + ex.Message, new RecordItemIndex(14)); }
            
            Report.Log(ReportLevel.Info, "Mouse", "Mouse Left Click item 'CounterDialog.BtnClose' at 29;17.", repo.CounterDialog.BtnCloseInfo, new RecordItemIndex(15));
            repo.CounterDialog.BtnClose.Click("29;17");
            Delay.Milliseconds(0);
            
            Report.Log(ReportLevel.Info, "Mouse", "Mouse Left Click item 'MainForm.MainPanel.Button111' at 11;10.", repo.MainForm.MainPanel.Button111Info, new RecordItemIndex(16));
            repo.MainForm.MainPanel.Button111.Click("11;10");
            Delay.Milliseconds(0);
            
            Report.Log(ReportLevel.Info, "Mouse", "Mouse Left Click item 'MainForm.MainPanel.Button111' at 11;10.", repo.MainForm.MainPanel.Button111Info, new RecordItemIndex(17));
            repo.MainForm.MainPanel.Button111.Click("11;10");
            Delay.Milliseconds(0);
            
            Report.Log(ReportLevel.Info, "Mouse", "Mouse Left Click item 'MainForm.ButtonTitle11' at 61;9.", repo.MainForm.ButtonTitle11Info, new RecordItemIndex(18));
            repo.MainForm.ButtonTitle11.Click("61;9");
            Delay.Milliseconds(0);
            
        }

#region Image Feature Data
#endregion
    }
#pragma warning restore 0436
}
