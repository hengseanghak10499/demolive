﻿///////////////////////////////////////////////////////////////////////////////
//
// This file was automatically generated by RANOREX.
// DO NOT MODIFY THIS FILE! It is regenerated by the designer.
// All your modifications will be lost!
// http://www.ranorex.com
//
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using System.Drawing;
using System.Threading;
using WinForms = System.Windows.Forms;

using Ranorex;
using Ranorex.Core;
using Ranorex.Core.Testing;
using Ranorex.Core.Repository;

namespace TestOnDemoLive
{
#pragma warning disable 0436 //(CS0436) The type 'type' in 'assembly' conflicts with the imported type 'type2' in 'assembly'. Using the type defined in 'assembly'.
    /// <summary>
    ///The Recording7 recording.
    /// </summary>
    [TestModule("907677a6-3e28-4638-9bab-6b83613b36dc", ModuleType.Recording, 1)]
    public partial class Recording7 : ITestModule
    {
        /// <summary>
        /// Holds an instance of the TestOnDemoLiveRepository repository.
        /// </summary>
        public static TestOnDemoLiveRepository repo = TestOnDemoLiveRepository.Instance;

        static Recording7 instance = new Recording7();

        /// <summary>
        /// Constructs a new instance.
        /// </summary>
        public Recording7()
        {
            username = "seanghak@oone.bz";
            password = "P123@hak";
            newpassword = "P123@oone";
            name = "";
            branchname = "អគ្គិសនីបន្ទាយមានជ័យ - មង្គលបុរី";
        }

        /// <summary>
        /// Gets a static instance of this recording.
        /// </summary>
        public static Recording7 Instance
        {
            get { return instance; }
        }

#region Variables

        string _username;

        /// <summary>
        /// Gets or sets the value of variable username.
        /// </summary>
        [TestVariable("44468856-b138-4a68-9b6b-e0fd0427d1a8")]
        public string username
        {
            get { return _username; }
            set { _username = value; }
        }

        string _password;

        /// <summary>
        /// Gets or sets the value of variable password.
        /// </summary>
        [TestVariable("3e368a12-21fc-4574-b503-6534a624c8d6")]
        public string password
        {
            get { return _password; }
            set { _password = value; }
        }

        string _newpassword;

        /// <summary>
        /// Gets or sets the value of variable newpassword.
        /// </summary>
        [TestVariable("898a6157-b9c2-431a-9357-18877d532ef5")]
        public string newpassword
        {
            get { return _newpassword; }
            set { _newpassword = value; }
        }

        string _name;

        /// <summary>
        /// Gets or sets the value of variable name.
        /// </summary>
        [TestVariable("37c593b7-84c1-452f-ad07-4e565fe28bf8")]
        public string name
        {
            get { return _name; }
            set { _name = value; }
        }

        string _branchname;

        /// <summary>
        /// Gets or sets the value of variable branchname.
        /// </summary>
        [TestVariable("3934a2c9-7791-41a2-957c-398a04e239fa")]
        public string branchname
        {
            get { return _branchname; }
            set { _branchname = value; }
        }

#endregion

        /// <summary>
        /// Starts the replay of the static recording <see cref="Instance"/>.
        /// </summary>
        [System.CodeDom.Compiler.GeneratedCode("Ranorex", global::Ranorex.Core.Constants.CodeGenVersion)]
        public static void Start()
        {
            TestModuleRunner.Run(Instance);
        }

        /// <summary>
        /// Performs the playback of actions in this recording.
        /// </summary>
        /// <remarks>You should not call this method directly, instead pass the module
        /// instance to the <see cref="TestModuleRunner.Run(ITestModule)"/> method
        /// that will in turn invoke this method.</remarks>
        [System.CodeDom.Compiler.GeneratedCode("Ranorex", global::Ranorex.Core.Constants.CodeGenVersion)]
        void ITestModule.Run()
        {
            Mouse.DefaultMoveTime = 300;
            Keyboard.DefaultKeyPressTime = 20;
            Delay.SpeedFactor = 1.00;

            Init();

            Report.Log(ReportLevel.Info, "Delay", "Waiting for 1s.", new RecordItemIndex(0));
            Delay.Duration(1000, false);
            
            Report.Log(ReportLevel.Info, "Set value", "Setting attribute Text to '$username' on item 'LoginDialog.Text'.", repo.LoginDialog.TextInfo, new RecordItemIndex(1));
            repo.LoginDialog.Text.Element.SetAttributeValue("Text", username);
            Delay.Milliseconds(0);
            
            Report.Log(ReportLevel.Info, "Set value", "Setting attribute Text to '$password' on item 'LoginDialog.Text1'.", repo.LoginDialog.Text1Info, new RecordItemIndex(2));
            repo.LoginDialog.Text1.Element.SetAttributeValue("Text", password);
            Delay.Milliseconds(0);
            
            Report.Log(ReportLevel.Info, "Mouse", "Mouse Left Click item 'LoginDialog.BtnLogin' at 51;7.", repo.LoginDialog.BtnLoginInfo, new RecordItemIndex(3));
            repo.LoginDialog.BtnLogin.Click("51;7");
            Delay.Milliseconds(0);
            
            Report.Log(ReportLevel.Info, "Delay", "Waiting for 300ms.", new RecordItemIndex(4));
            Delay.Duration(300, false);
            
            Report.Log(ReportLevel.Info, "Set value", "Setting attribute Text to '$password' on item 'ChangePasswordDialog.Container.Text'.", repo.ChangePasswordDialog.Container.TextInfo, new RecordItemIndex(5));
            repo.ChangePasswordDialog.Container.Text.Element.SetAttributeValue("Text", password);
            Delay.Milliseconds(0);
            
            Report.Log(ReportLevel.Info, "Set value", "Setting attribute Text to '$newpassword' on item 'ChangePasswordDialog.Container.Text1'.", repo.ChangePasswordDialog.Container.Text1Info, new RecordItemIndex(6));
            repo.ChangePasswordDialog.Container.Text1.Element.SetAttributeValue("Text", newpassword);
            Delay.Milliseconds(0);
            
            Report.Log(ReportLevel.Info, "Set value", "Setting attribute Text to '$newpassword' on item 'ChangePasswordDialog.Container.Text2'.", repo.ChangePasswordDialog.Container.Text2Info, new RecordItemIndex(7));
            repo.ChangePasswordDialog.Container.Text2.Element.SetAttributeValue("Text", newpassword);
            Delay.Milliseconds(0);
            
            Report.Log(ReportLevel.Info, "Mouse", "Mouse Left Click item 'ChangePasswordDialog.BtnSave' at 68;11.", repo.ChangePasswordDialog.BtnSaveInfo, new RecordItemIndex(8));
            repo.ChangePasswordDialog.BtnSave.Click("68;11");
            Delay.Milliseconds(0);
            
            Report.Log(ReportLevel.Info, "Mouse", "Mouse Left Click item 'SwitchBranchDialog.BranchCard' at 314;32.", repo.SwitchBranchDialog.BranchCardInfo, new RecordItemIndex(9));
            repo.SwitchBranchDialog.BranchCard.Click("314;32");
            Delay.Milliseconds(0);
            
            Report.Log(ReportLevel.Info, "Delay", "Waiting for 1s.", new RecordItemIndex(10));
            Delay.Duration(1000, false);
            
            try {
                //Report.Log(ReportLevel.Info, "Validation", "(Optional Action)\r\nValidating AttributeEqual (Text='ការចេញវិក្កយបត្រ') on item 'MainForm.PTopMenu.LblCaption1'.", repo.MainForm.PTopMenu.LblCaption1Info, new RecordItemIndex(11));
                //Validate.AttributeEqual(repo.MainForm.PTopMenu.LblCaption1Info, "Text", "ការចេញវិក្កយបត្រ", null, false);
                //Delay.Milliseconds(100);
            } catch(Exception ex) { Report.Log(ReportLevel.Warn, "Module", "(Optional Action) " + ex.Message, new RecordItemIndex(11)); }
            
            try {
                //Report.Log(ReportLevel.Info, "Validation", "(Optional Action)\r\nValidating AttributeEqual (Text='ការទទួលប្រាក់') on item 'MainForm.PTopMenu.LblCaption4'.", repo.MainForm.PTopMenu.LblCaption4Info, new RecordItemIndex(12));
                //Validate.AttributeEqual(repo.MainForm.PTopMenu.LblCaption4Info, "Text", "ការទទួលប្រាក់", null, false);
                //Delay.Milliseconds(100);
            } catch(Exception ex) { Report.Log(ReportLevel.Warn, "Module", "(Optional Action) " + ex.Message, new RecordItemIndex(12)); }
            
            try {
                Report.Log(ReportLevel.Info, "Validation", "(Optional Action)\r\nValidating AttributeEqual (Text='អតិថិជន') on item 'MainForm.PTopMenu.LblCaption5'.", repo.MainForm.PTopMenu.LblCaption5Info, new RecordItemIndex(13));
                Validate.AttributeEqual(repo.MainForm.PTopMenu.LblCaption5Info, "Text", "អតិថិជន", null, false);
                Delay.Milliseconds(100);
            } catch(Exception ex) { Report.Log(ReportLevel.Warn, "Module", "(Optional Action) " + ex.Message, new RecordItemIndex(13)); }
            
            try {
                Report.Log(ReportLevel.Info, "Validation", "(Optional Action)\r\nValidating AttributeEqual (Text='ថាមពល') on item 'MainForm.PTopMenu.LblCaption3'.", repo.MainForm.PTopMenu.LblCaption3Info, new RecordItemIndex(14));
                Validate.AttributeEqual(repo.MainForm.PTopMenu.LblCaption3Info, "Text", "ថាមពល", null, false);
                Delay.Milliseconds(100);
            } catch(Exception ex) { Report.Log(ReportLevel.Warn, "Module", "(Optional Action) " + ex.Message, new RecordItemIndex(14)); }
            
            try {
                //Report.Log(ReportLevel.Info, "Validation", "(Optional Action)\r\nValidating AttributeEqual (Text='ក្រុមហ៊ុន') on item 'MainForm.PTopMenu.LblCaption2'.", repo.MainForm.PTopMenu.LblCaption2Info, new RecordItemIndex(15));
                //Validate.AttributeEqual(repo.MainForm.PTopMenu.LblCaption2Info, "Text", "ក្រុមហ៊ុន", null, false);
                //Delay.Milliseconds(100);
            } catch(Exception ex) { Report.Log(ReportLevel.Warn, "Module", "(Optional Action) " + ex.Message, new RecordItemIndex(15)); }
            
            try {
                //Report.Log(ReportLevel.Info, "Validation", "(Optional Action)\r\nValidating AttributeEqual (Text='ការកំណត់') on item 'MainForm.PTopMenu.LblCaption6'.", repo.MainForm.PTopMenu.LblCaption6Info, new RecordItemIndex(16));
                //Validate.AttributeEqual(repo.MainForm.PTopMenu.LblCaption6Info, "Text", "ការកំណត់", null, false);
                //Delay.Milliseconds(100);
            } catch(Exception ex) { Report.Log(ReportLevel.Warn, "Module", "(Optional Action) " + ex.Message, new RecordItemIndex(16)); }
            
            try {
                //Report.Log(ReportLevel.Info, "Validation", "(Optional Action)\r\nValidating AttributeEqual (Text='របាយការណ៍') on item 'MainForm.PTopMenu.LblCaption7'.", repo.MainForm.PTopMenu.LblCaption7Info, new RecordItemIndex(17));
                //Validate.AttributeEqual(repo.MainForm.PTopMenu.LblCaption7Info, "Text", "របាយការណ៍", null, false);
                //Delay.Milliseconds(100);
            } catch(Exception ex) { Report.Log(ReportLevel.Warn, "Module", "(Optional Action) " + ex.Message, new RecordItemIndex(17)); }
            
            try {
                //Report.Log(ReportLevel.Info, "Validation", "(Optional Action)\r\nValidating AttributeEqual (Text='សុវត្ថិភាពប្រព័ន្ធ') on item 'MainForm.PTopMenu.LblCaption'.", repo.MainForm.PTopMenu.LblCaptionInfo, new RecordItemIndex(18));
                //Validate.AttributeEqual(repo.MainForm.PTopMenu.LblCaptionInfo, "Text", "សុវត្ថិភាពប្រព័ន្ធ", null, false);
                //Delay.Milliseconds(100);
            } catch(Exception ex) { Report.Log(ReportLevel.Warn, "Module", "(Optional Action) " + ex.Message, new RecordItemIndex(18)); }
            
            try {
                Report.Log(ReportLevel.Info, "Validation", "(Optional Action)\r\nValidating AttributeEqual (Text=$name) on item 'MainForm.TxtUserName'.", repo.MainForm.TxtUserNameInfo, new RecordItemIndex(19));
                Validate.AttributeEqual(repo.MainForm.TxtUserNameInfo, "Text", name, null, false);
                Delay.Milliseconds(100);
            } catch(Exception ex) { Report.Log(ReportLevel.Warn, "Module", "(Optional Action) " + ex.Message, new RecordItemIndex(19)); }
            
            try {
                Report.Log(ReportLevel.Info, "Validation", "(Optional Action)\r\nValidating AttributeEqual (Text=$branchname) on item 'MainForm.TxtBranch'.", repo.MainForm.TxtBranchInfo, new RecordItemIndex(20));
                Validate.AttributeEqual(repo.MainForm.TxtBranchInfo, "Text", branchname, null, false);
                Delay.Milliseconds(100);
            } catch(Exception ex) { Report.Log(ReportLevel.Warn, "Module", "(Optional Action) " + ex.Message, new RecordItemIndex(20)); }
            
            Report.Log(ReportLevel.Info, "Mouse", "Mouse Left Click item 'MainForm.LblUserDropDown' at 7;7.", repo.MainForm.LblUserDropDownInfo, new RecordItemIndex(21));
            repo.MainForm.LblUserDropDown.Click("7;7");
            Delay.Milliseconds(0);
            
            Report.Log(ReportLevel.Info, "Mouse", "Mouse Left Click item 'CnmStrip.ចកចញ' at 97;17.", repo.CnmStrip.ចកចញInfo, new RecordItemIndex(22));
            repo.CnmStrip.ចកចញ.Click("97;17");
            Delay.Milliseconds(0);
            
            Report.Log(ReportLevel.Info, "Delay", "Waiting for 1s.", new RecordItemIndex(23));
            Delay.Duration(1000, false);
            
        }

#region Image Feature Data
#endregion
    }
#pragma warning restore 0436
}
